=======
Credits
=======

Development Lead
----------------

* Rasmus Agren <rasmus.agren@scilifelab.se>
* Lena Hansson <lena.hansson@scilifelab.se>
* John Sundh <john.sundh@scilifelab.se>

Contributors
------------

Sergiu Netotea
