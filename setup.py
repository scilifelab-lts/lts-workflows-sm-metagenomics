#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import versioneer
from os.path import realpath, dirname, relpath, join
from setuptools import setup

ROOT = dirname(realpath(__file__))

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = [
    'pyyaml>=3.11',
    'lts-workflows',
]

test_requirements = [
    'snakemake>=3.10',
]

package_data = []

def package_path(path, filters=()):
    if not os.path.exists(path):
        raise RuntimeError("packaging non-existent path: %s" % path)
    elif os.path.isfile(path):
        package_data.append(relpath(path, 'lts_workflows_sm_metagenomics'))
    else:
        for path, dirs, files in os.walk(path):
            path = relpath(path, 'lts_workflows_sm_metagenomics')
            for f in files:
                if not filters or f.endswith(filters):
                    package_data.append(join(path, f))

suffixes = ('.rule', '.settings', '.sm', '.yaml', 'Snakefile', '.csv')
package_path(join(ROOT, "lts_workflows_sm_metagenomics"), suffixes)

setup(
    name='lts_workflows_sm_metagenomics',
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
    description="Metagenomics snakemake workflow",
    long_description=readme + '\n\n' + history,
    author="Rasmus Agren",
    author_email='rasmus.agren@scilifelab.se',
    url='',
    packages=[
        'lts_workflows_sm_metagenomics',
        'lts_workflows_sm_metagenomics.tests',
    ],
    package_dir={'lts_workflows_sm_metagenomics':
                 'lts_workflows_sm_metagenomics'},
    package_data={'lts_workflows_sm_metagenomics': package_data},
    include_package_data=True,
    install_requires=requirements,
    license="GNU General Public License v3",
    zip_safe=False,
    keywords='lts_workflows_sm_metagenomics',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],
    test_suite='lts_workflows_sm_metagenomics/tests',
    tests_require=test_requirements,
)
