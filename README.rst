===============================
lts_workflows_sm_metagenomics
===============================

.. image:: https://readthedocs.org/projects/lts-workflows-sm-metagenomics/badge/?version=latest
        :target: https://lts-workflows-sm-metagenomics.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status

.. image:: https://anaconda.org/percyfal/lts-workflows-sm-scrnaseq/badges/version.svg
	   :target: https://anaconda.org/percyfal/lts-workflows-sm-scrnaseq

.. image:: https://img.shields.io/badge/License-GPL%20v3-blue.svg
	   :target: http://www.gnu.org/licenses/gpl-3.0

Metagenomics snakemake workflow

Features
--------

* TODO

Credits
---------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
