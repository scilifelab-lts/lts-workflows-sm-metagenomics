# Preprocessing reads

**Note:** Read preprocessing can be run as a single part of the workflow using the command:

`snakemake --configfile <yourconfigfile> preprocess`

---

## Read trimming
Input reads can be trimmed using either [Trimmomatic](http://www.usadellab.org/cms/?page=trimmomatic)
or [Cutadapt](https://github.com/marcelm/cutadapt).
 
### Trimmomatic
The settings specific to Trimmomatic are:

`run_trimmomatic:`

Set to 'True' to preprocess reads using Trimmomatic.

`trimmomatic_home:`

The directory where Trimmomatic stores the .jar file and adapter sequences. If you don't know it
leave it blank to let the pipeline attempt to locate it.

`trim_adapters:`

Set to 'True' to perform adapter trimming.

`pe_adapter_params:` 

The adapter trim settings for paired end reads. This is what follows the 'ILLUMINACLIP' flag. The default "2:30:15" will look for seeds with
a maximum of **2** mismatches, and clip if extended seeds reach a score of **30** for paired-end reads or **10**.

`pe_pre_adapter_params:`

Trim settings to be performed prior to adapter trimming.

`pe_post_adapter_params:`

Trim settings to be performed after adapter trimming.

`se_adapter_params:`

`se_post_adapter_params:`

Same as above but for single-end reads.

### Cutadapt

## Read filtering
### Phix
`run_phix_filter`:

Set to True to filter out sequences mapping to the PhiX genome.

### SortMeRNA
[SortMeRNA](https://github.com/biocore/sortmerna) finds rRNA reads by
aligning to several rRNA databases. It can output aligning, rRNA, reads
and non-aligning, non_rRNA, reads to different output files allowing
you to filter your sequences.

`run_sortmerna:`

Set to `True` to filter your raw reads with SortMeRNA.

`keep:`

Sortmerna produces files with reads aligning to rRNA ('rRNA' extension)
and not aligning to rRNA ('non_rRNA') extension. With the `keep` setting
you specify which set of sequences you want to use for downstream analyses
('non_rRNA' or 'rRNA')

`remove_filtered:`

Set to True to remove the filtered reads (i.e. the reads NOT specified
in 'keep:')

`dbs:`

Databases to use for rRNA filtering. Can include:

- rfam-5s-database-id98.fasta
- rfam-5.8s-database-id98.fasta
- silva-arc-16s-id95.fasta
- silva-arc-23s-id98.fasta
- silva-bac-16s-id90.fasta
- silva-bac-23s-id98.fasta
- silva-euk-18s-id95.fasta
- silva-euk-28s-id98.fasta

`paired_strategy:`

How to handle read-pairs where mates are classified differently.

'paired_in' puts both reads in a pair into the 'rRNA' bin if one of
them aligns (i.e. more strict)

'paired_out' puts both reads in the 'other' bin

`params:`

Extra parameters to use for the sortmerna step.