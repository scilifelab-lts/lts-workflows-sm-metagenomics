# Documentation
Contents
--------
* [Annotation](annotation/index.md)
* [Assembly](assembly/assembly.md)
* [Binning](binning/binning.md)
* [Classifying reads](classification/index.md)
* [Configuration](configuration/index.md)
* [Preprocessing](preprocessing/preprocess.md)

## Overview
This is a snakemake workflow for processing and analysis of metagenomic
datasets. It can handle single- and paired-end data and can run on a
local laptop or in a cluster environment. The workflow will
run equally well on Linux and OSX, with the exception of the genome binning
steps which are currently only supported on Linux.

The source code is available at [BitBucket](https://bitbucket.org/scilifelab-lts/lts-workflows-sm-metagenomics)
 and is being developed as part of the [NBIS](http://nbis.se) bioinformatics infrastructure.

## Installation
### Clone the repository
Checkout the latest version of this repository (to your current directory):

```
git clone https://bitbucket.org/scilifelab-lts/lts-workflows-sm-metagenomics
```

### Install the required software
All the software needed to run this workflow is included as a [conda](http://anaconda.org) environment file.
To create the environment `sm-meta` use the supplied [environment.yaml](lts_workflows_sm_metagenomics/envs/environment.yaml) file
found in the *lts_workflows_sm_metagenomics/envs/* folder.

```
conda env create -f lts_workflows_sm_metagenomics/envs/environment.yaml
```

**Optional:** To install the software environment inside the workflow directory
(instead of in your home directory) you can run:

```
 mkdir envs/sm-meta
 conda env create -p envs/sm-meta -f envs/environment.yaml
 ```

This creates the `sm-meta` environment inside the `envs/` directory and
installs the environment there.

Next, add this directory to the envs_dirs in your conda config (this is to simplify
activation of the environment and so that the full path of the
environment installation isn't shown in your bash prompt):

```
conda config --add envs_dirs <full_path_to_repository>/envs/
```

Activate the environment using:

```
conda activate sm-meta
```

## Quick start guide
The default setup for the pipeline is to run **preprocessing** of reads, **assembly** and **annotation**, in that order.

We recommend that you copy the `default_configuration_parameters.yaml`
to some other file, e.g. 'myconfig.yaml', and make your changes in the copy.

```
cp default_configuration_parameters.yaml myconfig.yaml
```

Once you've created the [sample_list](http://nbis-metagenomic-workflow.readthedocs.io/en/latest/configuration/sample_list.html) and updated the corresponding `sample_list:`
path in the configuration file you're ready to start the pipeline using:

```
snakemake --configfile myconfig.yaml
```

```eval_rst
.. Note:: It's strongly recommended that you familiarize yourself with the configuration file and the various steps of the pipeline before starting a run.
```

## Using the example data
The `example-data` folder contains small datasets
that you can use to familiarize yourself with the pipeline.
 
A good way to get a grasp of things before actually doing something is to perform a 'dry-run':

```
snakemake --configfile default_configuration_parameters.yaml -np
```

This will print out all the different steps that would be run on your samples, without
actually running anything.

To actually run the pipeline on the example datasets remove the `-n` and run:

```
snakemake --configfile default_configuration_parameters.yaml -p
```

## Pipeline steps
If you don't want to run all the steps of this pipeline in one go you can
specify [targets](http://snakemake.readthedocs.io/en/stable/snakefiles/rules.html#targets)
which will cause snakemake to run only certain jobs. The available targets are
listed below. Click on the links to see a more detailed explanation and
related settings.

* __[annotation](http://nbis-metagenomic-workflow.readthedocs.io/en/latest/annotation/index.html)__: Annotation of coding sequences found on assembled contigs.
* __[assembly](http://nbis-metagenomic-workflow.readthedocs.io/en/latest/assembly/assembly.html)__: De-novo assembly of reads into contigs.
* __[binning](http://nbis-metagenomic-workflow.readthedocs.io/en/latest/binning/binning.html)__: Binning of metagenomic contigs.
* __[centrifuge_classify](http://nbis-metagenomic-workflow.readthedocs.io/en/latest/classification/centrifuge_classify.html)__: Read-based analysis against protein database using Kaiju.
* __[db](http://nbis-metagenomic-workflow.readthedocs.io/en/latest/annotation/db.html)__: Setup of databases for protein sequence annotation
* __[kaiju_classify](http://nbis-metagenomic-workflow.readthedocs.io/en/latest/classification/kaiju_classify.html)__: Read-based analysis against protein database using Kaiju.
* __[kraken_classify](http://nbis-metagenomic-workflow.readthedocs.io/en/latest/classification/kraken_classify.html)__: Read-based analysis using Kraken.
* __[preprocess](http://nbis-metagenomic-workflow.readthedocs.io/en/latest/preprocessing/preprocess.html)__: Preprocessing of reads.
* __[screening](http://nbis-metagenomic-workflow.readthedocs.io/en/latest/classification/screening.html)__: Comprehensive read-based analysis including identification of (ancient) taxa