# Classifying reads with Kraken

## How to build a new Kraken database
See [Building a new Kraken database](kraken_build_db.md) for instructions on how to configure the pipeline
to download the kraken database you want.
