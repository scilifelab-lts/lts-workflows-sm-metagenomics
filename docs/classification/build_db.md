# Building databases for read classification

## The Kraken database

The Kraken database is built with genomes downloaded in up to three different ways.

`kraken_custom_libraries:`
    
Dictionary with custom FASTA files that should be appended to the database. The
key is the library name which will be used in the database. The value is a list,
where the first element is the path, the second the pattern to download and the
third is an optional Bash command to use for filtering. The string plus space
is prepended to the file name, and the whole command must write results to stdout.
Use "" if not applicable. Any files matching the pattern will be assumed to contain
sequences in FASTA format. If the file name ends with ".gz" it will be decompressed
before being appended to the database.
    
For keys 'Viral', 'Archaea', 'Bacteria' and 'Plasmid' these should be the same as
the lower-case equivalents in kraken_libraries.
    
`kraken_libraries:`
    
This is used for the `kraken-build --download-library` command (refer to [Custom Databases](http://ccb.jhu.edu/software/kraken/MANUAL.html#custom-databases)
in the Kraken manual) and may contain any combination of "archaea", "bacteria", "plasmid","viral" and "human".
   
`kraken_assembly_lists:`
    
List of filenames with assemblies for sets of taxa. Each file is parsed and assemblies are sorted by level
of completeness keeping only the best one if there are several assemblies. In addition, only one genome is kept per
species if there are many strains and assemblies. Each assembly is then added to `kraken_custom_libraries`. 

`kraken_build_params:`

By default this is set to '--work-on-disk' to reduce the memory usage during the kraken-build step.
Set this to an empty string ("") if you believe that your system can handle the memory load.

## The Centrifuge database
The [Centrifuge](https://ccb.jhu.edu/software/centrifuge/) classifier generates its database in much the same way as Kraken.

`centrifuge_assembly_level:`

This is a list which may contain one or more of 'Complete Genome', 'Contig', 'Chromosome', 'Scaffold'.
Only genome assemblies matching one of the specified levels will be downloaded and added to the Centrifuge database.

`centrifuge_libraries:`

Similarly to Kraken, this is a list which may contain one or more of
bacteria, viral, archaea, fungi, protozoa, invertebrate, plant, vertebrate_mammalian, vertebrate_other.

Genomes matching the `centrifuge_assembly_level` (see above) will be downloaded from NCBI
for each set of taxa.

`centrifuge_assembly_lists:`

This is a dictionary with keys matching the `centrifuge_libraries` above but where the values are paths
to lists of assembly summaries for each taxa set. As for Kraken, each assembly list will be parsed and only one assembly per
taxonomic ID **and** assembly level will be downloaded.

## The Kaiju database
Currently, the Kaiju classification step will only use the same protein database that you have
specified for the taxonomic annotation of predicted protein coding sequences (see **Annotation**)
