# Classifying reads

This part of the workflow deals with classification of reads using k-mer based software
such as Kraken and Centrifuge but also the Kaiju classifier which performs protein-level
classification.

Contents
--------
* [Building a database](build_db.md)
* [Classifying reads with Kraken](kraken_classify.md)
* [Classifying reads with Centrifuge](centrifuge_classify.md)
* [Classifying reads with Kaiju](kaiju_classify.md)
* [Screening for taxa](screening.md)
