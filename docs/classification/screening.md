# Comprehensive screening of taxa

This part of the pipeline is intended to do comprehensive screening of taxa including
calculation of genome coverage and sequencing depth of identified taxa, filtering and
identification of ancient organisms. Therefore it is more comprehensive than the
[kraken_classify](http://nbis-metagenomic-workflow.readthedocs.io/en/latest/classification/kraken_classify.html)
and [centrifuge_classify](http://nbis-metagenomic-workflow.readthedocs.io/en/latest/classification/centrifuge_classify.html) parts of the pipeline,
although either Kraken and Centrifuge can be used as read-classifiers for screening.

## How to build a new database
See [Building databases for read classification](http://nbis-metagenomic-workflow.readthedocs.io/en/latest/classification/build_db.html) for instructions on how to configure the pipeline
to download the classification database you want.

## Configuration
Here are explanations of the specific configuration settings for the screening part of the workflow.

`tax_filter_name:`

This sets the name of a subfolder within the results, allowing you to
run classifications with different filtering parameters, such as coverage,
minimum number of k-mer to consider a match, ancient or not, etc.

`species_whitelist:`

Whitelist of NCBI taxonomy IDs to include when filtering classification results. Set to
`resources/ncbi/scientificName.tab` to include all species in the current NCBI taxonomy.

The workflow contains a number of rules which perform per species analyses, e.g.
mapping of reads to the relevant genome and generate coverage plots. Since the
number of species found in a metagenomic sample can be very large, there are several
filtering steps used to reduce the number of species to analyse more in detail.

Consider using a limited list of taxa if there are specific organisms you are interested in.

`min_kmer_count:`

Number of k-mers required to map to a taxa to have it pass initial filtering.
Note that for the centrifuge classifier, this is the number of reads assigned to a taxa.

`min_read_quality:`

For each alignment to individual genomes, only keep reads with a certain minimum quality.

`min_coverage_kept:`

After mapping reads to individual genomes, genome coverage will be calculated using bedtools
genomecov. This should be set very allowing as genomes with coverage below this value will not be reported in downstream analyses.

`bowtie_align_to_taxa:`

bowtie2 parameters used when aligning the potential reads for a given taxa
to the genome of that taxa.

`align_to_full_database:`

After reads have been aligned to the respective taxa of interest, you may also
want to get an idea of the specificity of matches. This is done by extracting reads
from the bam files for each taxa and aligning them to the full classification database.
Set this parameter to True if this is what you want to do.

`bowtie_align_to_full_database:`

bowtie2 parameters for aligning reads to the full classification database.


## Run modes
Identification of pre-defined taxa can be run in several *modes*:

**A) quick scan to identify samples, or runs, of interest**

Here are suggested settings for performing a quick scan:

```yaml
min_coverage_kept: 0.15
min_kmer_count: 10
species_whitelist: resources/ncbi/scientificName.tab
mapdamage_run_type: 'skip'
min_read_quality: 30
align_to_full_database: False
tax_filter_name: quick_modern_scan_of_all_taxa
```

**B) a more complete check to see if this is indeed the right taxa found**

(for example whilst looking for harmful species / pathogenic diseases)
The base recommendation would be to make a database with only these taxa and
run this in **parallel** to the default one. The default one will "handle"
if there are similar taxa, whereas the specific one will maybe be able to
"map" more reads.

```yaml
# change the align_to_full_database to True
align_to_full_database: True

# make a file containing the taxa found in the quick scan
species_whitelist: <full_path_here>

# update the folder name accordingly
tax_filter_name: qc_certain_taxa_in_XXX
```

**C: scan for ancient pathogens**

Take the **A) quick scan** configuration file and add/modify the below:

```yaml
min_kmer_count: 5

min_coverage_kept: 0.001

# do the Bayesian estimations for those that passes the quick cutoffs
mapdamage_run_type: complete

# only screen for pathogens
species_whitelist: resources/pathogens/pathogensFound.very_inclusive.tab

# update the folder name accordingly
tax_filter_name: ancient_pathogens
```


**D: quick scan for pathogens**

Take the **A) quick scan** configuration file and add/modify the below:

```bash
# set to 5 if you filter towards a smaller set (for example pathogens)
min_kmer_count: 5

# update the folder name accordingly
tax_filter_name: pathogens
```

**E: Diversity**

For diversity the pipeline should be run with relative allowing hit criteria,
and then you use the diversity reports as well as the Krona reports
for each sample to look through the data. Here you want as including
database as possible. **However**, be aware that the Kraken and Centrifuge
databasese take up a lot of disk space.

```yaml
# Lower the 'min_coverage_kept' parameter to 0.01 in order to include more hits
min_coverage_kept 0.01
```
