#!/usr/bin/env python

import os, sphinx_bootstrap_theme
from recommonmark.transform import AutoStructify
from recommonmark.parser import CommonMarkParser

cwd = os.getcwd()
project_root = os.path.dirname(cwd)

source_suffix = ['.rst', '.md']
source_parsers = {'.md': CommonMarkParser}

# Activate the theme.
html_theme = 'bootstrap'
html_theme_path = sphinx_bootstrap_theme.get_html_theme_path()
html_theme_options = {
    'navbar_title': "NBIS-metagenomics",
    'navbar_sidebarrel': False,
    'source_link_position': "footer",
    'navbar_links': [("Bitbucket", "https://bitbucket.org/scilifelab-lts/lts-workflows-sm-metagenomics", 1)]
}
html_static_path = ["_static"]
html_logo = "nbis.png"
html_title = "NBIS metagenomics workflow"
html_short_title = "NBIS-metagenomics"

master_doc = 'index'

github_doc_root = 'https://bitbucket.org/scilifelab-lts/lts-workflows-sm-metagenomics/src/0031aeec24755156cf9a063072e26a2a4052767d/docs/'


def setup(app):
    app.add_config_value('recommonmark_config', {
            'url_resolver': lambda url: github_doc_root + url,
            'auto_toc_tree_section': 'Contents',
            'enable_eval_rst': True,
            'enable_auto_toc_tree': True,
            'enable_auto_doc_ref': True
            }, True)
    app.add_transform(AutoStructify)