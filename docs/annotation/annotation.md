# Annotation of coding sequences

**Note:** Annotation of protein coding sequences can be run as a single part of the workflow using the command:

`snakemake --configfile <yourconfigfile> annotation`


## Protein family annotation
Annotation of predicted protein coding sequences is performed using
hmmsearch from the [HMMER](http://hmmer.org/) package. In the config file
the `hmmsearch:` section specifies how to annotate sequences.

`run_hmmsearch:`

Set to True in order to perform any annotation using hmmsearch.

`hmmdbs:`

This is a list which may contain any combination of 'TIGRFAM', 'PFAM'
and 'EGGNOG'.

```eval_rst
.. note:: Enzymes are inferred for protein sequences using information from the EGGNOG database. So in order to annotate sequences with enzyme EC numbers and quantify metabolic pathways you need to include 'EGGNOG' in the list of hmmsearch databases
```

`tigrfam_threads:`

`pfam_threads:`

`eggnog_threads:`

The parameters above specify how many threads to use for hmmsearch against the
different databases. Annotation with EGGNOG is more resource demanding and
if possible should be run with more threads.

`dbpath:`

Path where the HMM files for the databases will be stored.

## Taxonomic annotation
`taxonomic_annotation:`

Taxonomic annotation of protein sequences is performed by:
1. similarity searches against a reference database
2. parsing best hits for each protein query and linking hits to taxonomic IDs
3. assigning a taxonomic label to the lowest common taxa for the 'best hits'

(Actually in this workflow, contigs are assigned a taxonomy based on
the lowest common ancestor of the classified proteins on the contig,
and then this contig-taxonomy is applied to all proteins on the contig)

The first step above is performed in this workflow using
[diamond](https://github.com/bbuchfink/diamond/) against a protein sequence
reference database (see more on this below).

`run_taxonomic:`

Set to True to run the diamond search step and taxonomic assignment
of protein sequences using the database of your choice.

`diamond_dbpath:`

Path to where the diamond formatted database will be stored.

`dbtype:`

Can be one of 'nr', 'uniref50', 'uniref90', 'uniref100'

### Methodology

The assignment of taxonomy in the second and third steps is based on
a method originally introduced in
the [DESMAN](https://github.com/chrisquince/DESMAN) package. Briefly this
is done as follows: hits reported by diamond are weighted (ranging from 0-1)
based on the alignment fraction and %identity of the hit. The method then
attempts to assign a taxonomy for each query protein, starting at the
species level and then moving up to higher taxonomic ranks. At each rank,
only hits with weights exceeding a rank-specific threshold are considered.
By default these thresholds are set at: species = 0.95, genus = 0.9,
family = 0.8, order = 0.7, class = 0.6, phylum = 0.5, superkingdom = 0.4.
At each rank, if the sum of weights for one taxon is greater than half the
total sum of weights then that taxon is assigned to the query. Higher
taxonomic ranks (if any) are inferred directly from that taxon while
lower ranks are given the taxon name prefixed with "Unclassified." (e.g.
Unclassified.Proteobacteria").

The plot below shows how the weights of hits relates to the %id of the
hit as well as the alignment fraction of the query to the hit.



```eval_rst
.. image:: ../img/taxonomy_weights.png
    :width: 600
    :alt: Weighted hits plot
```


### Deciding which blast database to use
Ideally we'd like to assign species-level taxonomy to all the protein sequences in our analysis but metagenomics 
doesn't often allow that. This is partly a database/data issue since reference databases are not good representatives of
the taxa being studied. A lot of times the best taxonomic annotation a protein sequence can get is at genus level.

Since the similarity search step (which uses [diamond](https://github.com/bbuchfink/diamond)) can be a bottleneck it 
may be worthwhile to use a **clustered** reference database such as [UniRef](http://www.uniprot.org/help/uniref). 
In UniRef, protein sequences in the UniProt database are clustered by % sequence identity using the 
[CD-HIT](http://weizhongli-lab.org/cd-hit/) software and a *seed* sequence is selected for each cluster. 

For instance, in UniRef90 sequences are clustered at 90% sequence identity. In the 2017_08 release of UniProt/UniRef 
this reduces the number of sequences from 89,951,742 in the UniProt database to 59,207,328 in the UniRef90 database.
Consequently, searching the UniRef90 database instead of the full UniProt database (or the NCBI nr database) will be a 
lot faster. The drawback is that the taxonomic annotation may not be as sensitive.

Conveniently, each entry in UniRef database contains information on the lowest common taxa for all sequences contained
in the cluster. So we can evaluate the number of unique taxa at each taxonomic rank in the databases.

Below is a comparison of unique taxa at the genus and species level
for Archaea, Bacteria, Eukaryota and Viruses in all the UniRef databases
as well as in the full NCBI non-redundant database.

 ```eval_rst
.. image:: ../img/tax_database_comparison.png
    :width: 400
    :alt: Blast databases
```

## Detecting non coding RNA with Infernal
[Infernal](http://eddylab.org/infernal/) is a tool that uses *covariance models* (CMs)
to identify sequences matching RNA families (defined in the [Rfam](http://rfam.xfam.org/)
database). Run this tool to identify such stretches on contigs in the assemblies.

```eval_rst
.. note:: Running infernal and parsing the output can be quite time-consuming.
```

`run_infernal:`

Set to True to analyze assembled contigs with infernal.

`threads:`

Number of threads to use for infernal

`dbpath:`

Path to store the infernal database.