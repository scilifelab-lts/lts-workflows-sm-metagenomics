# Generating required database files

Several steps of this workflow requires large database files that may take
a long time to download and format. If you want to can run the database
creation separately with the workflow (e.g. while you're waiting for real
data to arrive).

To create the databases needed for the protein annotation steps you can run:

```
snakemake --configfile <your-config-file> -p db
```

The exact list of jobs will depend on settings in your configuration file. In the
default setup you will see:

```
snakemake --configfile default_configuration_parameters.yaml -np db

<lots of text>

Job counts:
	count	jobs
	1	cog2ec
	1	db
	1	db_done
	1	download_eggnog
	1	download_uniref
	1	get_kegg_module_mapping
	1	prepare_diamond_db_uniref
	1	prepare_taxfiles_uniref
	8
```

```eval_rst
.. note:: The 'db' target only includes database files used in preprocessing (SortMeRNA) and protein annotation.
```

```eval_rst
.. seealso:: See the documentation for ways to create databases for `read-classification <http://nbis-metagenomic-workflow.readthedocs.io/en/latest/classification/build_db.html>`_.
```
