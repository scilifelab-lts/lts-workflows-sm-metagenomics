# Protein annotation

This section deals with how to annotate protein sequences predicted on
assembled contigs.

Contents
--------
* [Annotating protein sequences](annotation.md)
* [Databases for annotation](db.md)