# Binning metagenomic contigs

```eval_rst
.. Note:: The binning step of the workflow is currently only supported on Linux.
```

Assembled contigs can be grouped into so called 'genome bins' using information
about their nucleotide composition and their abundance profiles in several
samples. The rationale is that contigs from the same genome will have similar
nucleotide composition and will show up in similar abundances across
samples.

There are several programs that perform unsupervised clustering of contigs
based on composition and coverage. Currently, this workflow only uses
[MaxBin2](https://downloads.jbei.org/data/microbial_communities/MaxBin/MaxBin.html)
but other popular similar tools are [CONCOCT](https://github.com/BinPro/CONCOCT/)
and [MetaWatt](https://sourceforge.net/projects/metawatt/).

The quality and phylogeny of bins can also be estimated as part of the workflow, using
[CheckM](https://github.com/Ecogenomics/CheckM).

To perform the binning step of the workflow run the following:

```
snakemake --configfile <your-config-file> -p binning
```

```eval_rst
.. seealso:: Check out the binning tutorial further down in this document.
```

## Settings

### MaxBin2
`run_binning:`

Set to True to perform binning on all assembly groups in the sample annotation file

`map_dir:`

Path to save output from cross-mapping of reads to contigs. Bam files are marked as
temporary in the workflow and thus the map_dir will not take up a huge amount of space once
the abundance files are prepared.

`bin_dir:`

Path to save output from maxbin2. Each binned assembly will have a sub-folder under
the bin_dir directory. In each sub-folder a nucleotide fasta file will be generated for each genome
bin with the filename pattern "{assemblyGroup}.nnn.fasta" where 'nnn' is a number starting
at 001. The output folder will also contain a log file from the binning, a summary file
with some stats on each bin, a list of contigs that could not be binned as well as a list
of contigs that were too short (below the min_contig_length specified).

```eval_rst
.. Note:: The completeness estimate in MaxBin2 does not work properly so don't believe the completeness values in the .summary file. Instead run the CheckM step of the workflow to get better estimates.
```

`min_contig_length:`

Minimum length of contigs to be binned. Shorter contigs will be filtered out.


`threads:`

Number of threads to use for the MaxBin2 step.

### CheckM
`run_checkm:`

Set to True to analyze bins with Checkm.

`dir:`

Resource path (marker files and genome trees) for Checkm.

`marker_taxon:`

The taxon marker file to use. Defaults to 'Prokaryote'
but if you know that your bins are from only a specific taxa (e.g. 'Bacteria'
or 'Alphaproteobacteria') you can specify that here. Run `checkm taxon_list`
to get a listing of all possible marker files.

`marker_rank:`

 The rank of the specified marker_taxon. For 'Prokaryote' the marker_rank is 'life'.
 For 'Bacteria' the rank is 'domain'.

`reduced_tree:`

Set to Tru to run the phylogenetic analysis of bins with a reduced tree. This
will speed up the analysis but may produce slightly worse taxonomic assignments.

`tree_params:`

Here you can specify additional parameters for the `checkm tree` step.

`good_bin_threshold:`

Here you can specify threshold for how to define 'good quality bins'

`completeness:`

Minimum estimated completeness (in %) to classify a bin as good quality.

`contamination:`

Maximum estimated contamination (in %) for good quality bins.

`max_genome_size:`

Maximum genome size (in bp) for good quality bins.

`min_genome_size:`

Minimum genome size (in bp) for good quality bins.

## Binning tutorial
When the binning step is included in the workflow all assembly groups
specified in the [sample annotation file](http://nbis-metagenomic-workflow.readthedocs.io/en/latest/configuration/sample_list.html)
will be binned and for each assembly the abundances of contigs will be
calculated by cross-mapping all samples in the sample annotation file.

This means that if you have many assemblies and/or samples there will be
a lot of jobs to run. So while you *can* run assembly and binning in one
go you may want to get a better overview by first running the [assembly](http://nbis-metagenomic-workflow.readthedocs.io/en/latest/assembly/assembly.html)
step and then bin individual assemblies.

Let's try this out using some supplied example data. The configuration
file `config/binning_example.yaml` is set up to use four mock communities created with this
[metagenomic-mocks](https://bitbucket.org/johnne/metagenomic-mocks) repository.
The samples each contain 100 000 reads sampled from the same 10 genomes but in varying proportions,
roughly simulating the proportions of the genomes in different body sites.

### Generate the assemblies

```
snakemake --configfile config/binning_example.yaml -p assembly
```

### Bin each assembly

*Anterior nares:*

```
snakemake --configfile config/binning_example.yaml -p bin_example/progress/anterior_nares.binning.done
```

*Buccal mucosa:*

```
snakemake --configfile config/binning_example.yaml -p bin_example/progress/buccal_mucosa.binning.done
```

*Retroauricular crease:*

```
snakemake --configfile config/binning_example.yaml -p bin_example/progress/retr_crease.binning.done
```

*Stool:*

```
snakemake --configfile config/binning_example.yaml -p bin_example/progress/stool.binning.done
```

*Co-assembly:*

```
snakemake --configfile config/binning_example.yaml -p bin_example/progress/all.binning.done
```

You will see that the assemblies generated 2 genome bins each.

```
ls -l bin_example/bins/*/*.fasta
```

Of course, with real data you will likely end up with a lot more genome bins.

### Checking the quality of bins

```eval_rst
.. important:: Don't forget the '--use-conda' flag to snakemake when the workflow contains rules using checkm.
```

Now that we have binned each assembly we can use CheckM to estimate bin quality:

For anterior nares:

```
snakemake --use-conda --configfile config/binning_example.yaml -p bin_example/bins/anterior_nares/checkm/anterior_nares.genome_stats.tab
```

This produces a file with genome statistics for each bin in the assembly.
A quick look at the resulting file:

```
cat bin_example/bins/anterior_nares/checkm/anterior_nares.genome_stats.tab | cut -f1,6,7,9
```

shows that the two bins have very low estimated completeness and genome sizes < 500 kb.
This is not surprising given the low number of reads in the mock communities.

Let's generate the same files for the other bins:

```
snakemake --use-conda --configfile config/binning_example.yaml -p bin_example/bins/buccal_mucosa/checkm/buccal_mucosa.genome_stats.tab
snakemake --use-conda --configfile config/binning_example.yaml -p bin_example/bins/retr_crease/checkm/retr_crease.genome_stats.tab
snakemake --use-conda --configfile config/binning_example.yaml -p bin_example/bins/stool/checkm/stool.genome_stats.tab
snakemake --use-conda --configfile config/binning_example.yaml -p bin_example/bins/all/checkm/all.genome_stats.tab
```

**Genome completeness** is estimated from presence/absence of marker genes in each
genome bin. By default, checkm uses a set of 56 marker genes present in prokaryotic
genomes (you can use the configuration file to have checkm run with more lineage specific
marker genes via the `marker_taxon` and `marker_rank` settings). **Contamination**
is estimated by looking at differences in amino acid sequences of the identified markers in
each bin.

#### Extracting good quality bins
Based on the bin quality control in the previous step you can use the workflow
to directly extract bins that are 'good quality' based on genome completeness and
contamination. By default, 'good bins' are defined as having 80% or higher genome
completeness and 5% or lower contamination.
To extract good bins from the results you can run (for anterior nares):

```
snakemake --use-conda --configfile config/binning_example.yaml -p bin_example/bins/anterior_nares/good_bins/anterior_nares.good_bins.tab
cat bin_example/bins/anterior_nares/good_bins/anterior_nares.good_bins.tab
```

As you can see, with the default definition of 'good quality' there are no bins meeting the threshold.
You can try changing the thresholds in the config file and rerunning the command:

```
sed -i 's/completeness: [0-9]\+/completeness: 10/g' config/binning_example.yaml
snakemake --use-conda --configfile config/binning_example.yaml -p bin_example/bins/anterior_nares/good_bins/anterior_nares.good_bins.tab
cat bin_example/bins/anterior_nares/good_bins/anterior_nares.good_bins.tab
```

### Checking phylogeny of bins

CheckM uses [pplacer](https://matsen.github.io/pplacer/) to place bins into
a reference tree and estimate the best phylogenetic assignment. The resulting
phylogeny table shows the lowest assigned taxonomy for each bin.

*Anterior nares:*

```
snakemake --use-conda --configfile config/binning_example.yaml -p bin_example/bins/anterior_nares/checkm_tree/anterior_nares.phylogeny.tab
```

*Buccal mucosa:*

```
snakemake --use-conda --configfile config/binning_example.yaml -p bin_example/bins/buccal_mucosa/checkm_tree/buccal_mucosa.phylogeny.tab
```

*Retroauricular crease:*

```
snakemake --use-conda --configfile config/binning_example.yaml -p bin_example/bins/retr_crease/checkm_tree/retr_crease.phylogeny.tab
```

*Stool:*

```
snakemake --use-conda --configfile config/binning_example.yaml -p bin_example/bins/stool/checkm_tree/stool.phylogeny.tab
```

*Co-assembly:*

```
snakemake --use-conda --configfile config/binning_example.yaml -p bin_example/bins/all/checkm_tree/all.phylogeny.tab
```

Take a look at the resulting phylogeny tables:

```
cat bin_example/bins/*/checkm_tree/*.tab
```

As you can see, the **anterior nares** and **buccal mucosa** bins are classified
at the species level as *Propionibacterium acnes* or *Streptococcus mitis*,
respectively (although one of the bins in buccal mucosa could only be classified
at genus level). This is expected since in anterior nares (nose), *P. acnes* was set to
be the dominant genome at 30% (the next most abundant genome *Corynebacterium kroppenstedtii*
being present at 15%), and in buccal mucosa (mouth) *S. mitis* was set to 75%.
In the **retroauricular crease** (ear) the two bins were classified as *P. acnes* and
and the genus Corynebacterium. In this sample *Corynebacterium kroppenstedtii* was
and *P. acnes* were both sampled at 30%. The **stool** sample had *Prevotella copri* sampled
at 75%, matching nicely with the phylogenetic placement of the bins. Finally, the
co-assembly had bins classified as species *P. acnes* and genus Streptococcus.

```eval_rst
.. note:: The low number of reads used for the samples in this tutorial is likely the reason why no sample had higher completion rates for the bins. Our tests show that subsampling to 10^6 reads give substantially better binning results.
```