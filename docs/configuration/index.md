# Configuration
Here you'll find information on how to configure the pipeline and how to format the required input.

Contents
--------
* [Config parameters](config_parameters.md)
* [The sample list](sample_list.rst)
* [Running on a cluster](running_on_a_cluster.md)
* [Under the hood](under_the_hood.md)