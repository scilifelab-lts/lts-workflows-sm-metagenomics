
The sample list file
====================

``sample_list: "samples/sample_annotation_example.tab"``

The `sample_list:` config parameter should point to a file containing information about the samples and respective
files to be used as input for the pipeline.

The sample list file specifies where the input data for the workflow is located on your computer or
the compute cluster you are running on. 

Below is an example of what the sample file may look like. This
example table is also found in [sample_annotation_example.tab](../../lts_workflows_sm_metagenomics/samples/sample_annotation_example.tab).
This example uses data from the `Human Microbiome Project <http://hmpdacc.org/>`_ subsampled to 10k reads.

========  =====  =============  ===================================   =================================
sampleID  runID  assemblyGroup            fileName                              pair
========  =====  =============  ===================================   =================================
  gut       1       gut,all     example-data/gut_R1.10k.fastq.gz      example-data/gut_R2.10k.fastq.gz
  skin      1       skin,all    example-data/skin_R1.10k.fastq.gz     example-data/skin_R2.10k.fastq.gz
  skin      2       skin,all    example-data/skin_2_R1.10k.fastq.gz
========  =====  =============  ===================================   =================================

**The sampleID and runID columns:**

The *sampleID* column allows you to designate a sample ID for each set of sequences while the *runID* column can
be used to designate e.g. technical replicates of samples. These two columns together form a unique tag for each
sequence set. If there is only one sequencing run per sampleID you may leave the runID column empty or simply
fill in a '1'. In the example above there are two samples, designated sample1 and sample2, with a single run for
sample1 and two runs for sample2.

**The assemblyGroup column:** 

The *assemblyGroup* column allows you to group together samples (and/or individual
sample runs) into assembly groups. A single sample/run combination can be grouped into multiple assembly groups by
specifying comma separated assembly group names in this field. In the example above gut is assigned to the assembly
groups 'gut' and 'all', and skin is assigned to the assembly groups 'skin' and 'all'. Running the workflow
with this file will produce three assemblies called 'gut', 'skin' and 'all' where the 'all' assembly will be a
co-assembly using all the input reads.  

**The fileName and pair columns:** 

These two columns specify file paths for sequences in the (gzipped) fastq format.
For paired end data the *fileName* column points to *forward* read file and the *pair* column points to the
corresponding *reverse* read file. For single end data only the *fileName* column is
used. In the example above gut has the *forward* reads file in `example-data/gut_R1.10k.fastq.gz` and the *reverse* reads
file in `example-data/gut_R2.10k.fastq.gz`. In the example these paths are relative to the root directory of this repository but
they may also point to absolute paths somewhere else in the file system. Note that for run 2 of the skin sample the *pair*
field is left empty because that is a single end sequencing run and only the *forward* reads are available.    
