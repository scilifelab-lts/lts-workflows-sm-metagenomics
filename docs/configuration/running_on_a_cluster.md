# How to run on UppMax/Hebbe (SNIC resources)
Copy the `cluster.yaml` file to `mycluster.yaml`.

```eval_rst
 .. important:: update the 'account:' to contain the right account number (most likely in the format SNIC2017-1-123).
```

Now you can run snakemake on the cluster using:

```
snakemake -r -k -j 80 --configfile configuration.yaml --cluster-config mycluster.yaml --cluster "sbatch -A {cluster.account} -t {cluster.time} -p {cluster.partition} -n {cluster.n} -e {cluster.log} -o {cluster.log} {cluster.extra}"
```

You can specify targets just as you would normally. Slurm log files are
placed under `logs/` named in the format `snakejob.<rule_name>.nn.sh-<slurm-job-id>-<username>.log`