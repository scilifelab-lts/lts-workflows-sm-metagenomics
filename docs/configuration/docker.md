### To run via docker
- make a run_pipeline.sh script as the below instructions
```bash
bash run_pipeline.sh
```

#### Docker pipeline script

```bash
# in the sampleFolder there is a file called sample_annotation.tab - in addition to the actual fastq files :)
sampleFolder="<full_path_to_a_folder_on_your_system>"

# which version of the kraken database will be used? if it already exists use it, otherwise download and create it
krakenDatabase="<the_path_where_an_existing_kraken_exists_or_will_be_created>"

# where should the output be stored? - in this folder make a pipeline_config.yaml file
resultsFolder="<once_the_run_is_finished_where_should_the_results_be_copied_into>"
mkdir -p ${resultsFolder}

# which pathogen list should be used as a filter? and which species should be included in the generation of the kraken database
taxFolder="<full_path_to_a_folder_on_your_system>"

# where to store temporary results? preferably a fast disk without backup
tempFolder="<full_path_to_a_folder_on_your_system"
mkdir -p ${tempFolder}

# download the latest image (if newer than current) and then open (run) an interactive terminal (it)
# once the docker container is spun up snakemake -n -r will show all the jobs that needs to be done
# to run without a slurm system something like
#    snakemake -j 36 is suggested
# to run on a slurm system something like
#    snakemake -r -j 80 --cluster-config cluster.yaml --cluster "sbatch -A {cluster.account} -t {cluster.time} -p {cluster.partition} -n {cluster.n} {cluster.extra}"

docker pull <UPDATE_RIGHT_NAME_HERE> \
&& docker run --rm -it \
  -v ${taxFolder}:/home/<UPDATE_DOCKER_FOLDER_NAME_HERE>/data \
  -v ${resultsFolder}:/home/<UPDATE_DOCKER_FOLDER_NAME_HERE>/results \
  -v ${tempFolder}:/home/<UPDATE_DOCKER_FOLDER_NAME_HERE>/temp \
  -v ${krakenDatabase}:/home/<UPDATE_DOCKER_FOLDER_NAME_HERE>/db_v1 \
  -v ${sampleFolder}:/home/<UPDATE_DOCKER_FOLDER_NAME_HERE>/samples \
  -v ${resultsFolder}pipeline_config.yaml:/home/<UPDATE_DOCKER_FOLDER_NAME_HERE>/configuration.yaml \
  -e LOCAL_USER_ID=`id -u $USER` \
  <UPDATE_RIGHT_NAME_HERE>
```