# Configuration parameters

The file `default_configuration_paramters.yaml` contains all the parameters for the pipeline.
The most general settings are explained here. For more specific settings
regarding the different workflow steps you may refer to the corresponding documentation.

## Paths

`workdir:`

Working directory for the snakemake run. All paths are evaluated relative to this directory.

`sample_list: `

should point to a file with information about samples. See the [sample list](http://nbis-metagenomic-workflow.readthedocs.io/en/latest/configuration/sample_list.html)
 documentation for more details on the format of this file.
 
 `results_path: `
 
this directory will contain reports, figures, count tables and other types of 
aggregated and processed data. Output from the different steps will be created in 
sub-directories. For instance, assemblies will be in a sub-directory called 'assembly'

`intermediate_path: `

this directory contains files which might have required a substantial computational 
effort to generate and which might be useful for further analysis. They are not deleted 
by Snakemake once used by downstream rules. This could be for example bam files.

`temp_path: `

this directory contains files which are either easily regenerated or redundant. This 
could be for example sam files which are later compressed to bam.

`scratch_path: `

Similar to temp_path but primarily used on cluster resources (e.g. Uppmax)
in order to speed up writes to disk by utilizing local storage for nodes.

`resource_path: `

Path to store database files needed for the workflow.

`report_path:`

Path to store report files such as assembly and mapping stats and plots