localrules: avg_seq_length_pe, avg_seq_length_se

def get_interleaved(sample,runID):
  files = []
  if "interleaved" in samples[sample][runID].keys():
    inter = samples[sample][runID]["interleaved"]
    R1 = samples[sample][runID]["R1"]
    R2 = samples[sample][runID]["R2"]
    files.append(inter)
  else:
    files.append("")
  return files

rule deinterleave_fastq:
  input:
    lambda wildcards: get_interleaved(wildcards.sample,wildcards.run)
  output:
    R1=os.path.join(config["intermediate_path"],"deinterleaved","{sample}_{run}_R1.fastq.gz"),
    R2=os.path.join(config["intermediate_path"],"deinterleaved","{sample}_{run}_R2.fastq.gz")
  params:
    script="source/utils/deinterleave_fastq.sh",
    tmp_r1=os.path.join(os.path.expandvars(config["scratch_path"]),"{sample}_{run}_R1.fastq.gz"),
    tmp_r2=os.path.join(os.path.expandvars(config["scratch_path"]),"{sample}_{run}_R2.fastq.gz")
  run:
    for item in input:
      if not item:
        continue
      shell("echo {params.script} {item} {params.tmp_r1} {params.tmp_r2} compress")
      shell("{params.script} {item} {params.tmp_r1} {params.tmp_r2} compress")
      shell("mv {params.tmp_r1} {output.R1}")
      shell("mv {params.tmp_r2} {output.R2}")

def get_len(line):
  line = (line.rstrip()).lstrip()
  items = line.split(" ")
  l = [int(items[1])]*int(items[0])
  return l

rule avg_seq_length_pe:
  input:
    R1 = os.path.join(config["intermediate_path"],"preprocess","{sample}_{run}_R1"+PREPROCESS+".fastq.gz"),
    R2 = os.path.join(config["intermediate_path"],"preprocess","{sample}_{run}_R2"+PREPROCESS+".fastq.gz")
  output:
    os.path.join(config["intermediate_path"],"preprocess","{sample}_{run}_pe.read_length.txt")
  run:
    import numpy as np
    lengths = []
    for line in shell("seqtk seq -f 0.01 {input.R1} | seqtk comp | cut -f2 | sort | uniq -c", iterable = True):
      lengths += get_len(line)
    for line in shell("seqtk seq -f 0.01 {input.R2} | seqtk comp | cut -f2 | sort | uniq -c", iterable = True):
      lengths += get_len(line)
    sample_length = np.round(np.mean(lengths),2)
    with open(output[0], 'w') as fh:
      fh.write("\tavg_len\n")
      fh.write("{}_{}\t{}\n".format(wildcards.sample,wildcards.run,sample_length))

rule avg_seq_length_se:
  input:
    se = os.path.join(config["intermediate_path"],"preprocess","{sample}_{run}_se"+PREPROCESS+".fastq.gz")
  output:
    os.path.join(config["intermediate_path"],"preprocess","{sample}_{run}_se.read_length.txt")
  run:
    import numpy as np
    lengths = []
    for line in shell("seqtk seq -f 0.01 {input.se} | seqtk comp | cut -f2 | sort | uniq -c", iterable = True):
      lengths += get_len(line)
    sample_length = np.round(np.mean(lengths),2)
    with open(output[0], 'w') as fh:
      fh.write("\tavg_len\n")
      fh.write("{}_{}\t{}\n".format(wildcards.sample,wildcards.run,sample_length))