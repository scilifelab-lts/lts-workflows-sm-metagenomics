wildcard_constraints:
  seq_type = "[ps]e"

rule remove_mark_duplicates:
  input:
    os.path.join(config["results_path"],"assembly","{group}","mapping","{sample}_{run}_{seq_type}.bam")
  output:
    os.path.join(config["results_path"],"assembly","{group}","mapping","{sample}_{run}_{seq_type}.markdup.bam"),
    os.path.join(config["results_path"],"assembly","{group}","mapping","{sample}_{run}_{seq_type}.markdup.metrics")
  log:
    os.path.join(config["results_path"],"assembly","{group}","mapping","{sample}_{run}_{seq_type}.markdup.log")
  params:
    temp_bam=os.path.join(config["tmpdir"],"{group}-mapping-{sample}_{run}_{seq_type}.markdup.bam"),
    temp_sort_bam=os.path.join(config["tmpdir"],"{group}-mapping-{sample}_{run}_{seq_type}.markdup.re_sort.bam"),
    jarfile=config["picard"]["picard_jar"],
    picard_path=config["picard"]["picard_path"],
    java_opt="-Xms2g -Xmx31g"
  threads: 4
  shell:
    """
    java {params.java_opt} -XX:ParallelGCThreads={threads} -cp params.picard_path -jar {params.jarfile} MarkDuplicates \
      I={input} O={params.temp_bam} M={output[1]} ASO=coordinate REMOVE_DUPLICATES=TRUE 2> {log}
    # Re sort the bam file using samtools
    samtools sort -@ 3 -o {params.temp_sort_bam} {params.temp_bam}
    mv {params.temp_sort_bam} {output[0]}
    rm {params.temp_bam}
    """