localrules: write_htseq_gff, sum_to_taxa, aggregate_htseq, make_gene_id_map, sum_to_features, quantify_kegg_pathways, taxonomy2krona

rule write_featurefile:
  input:
    os.path.join(config["results_path"],"annotation","{group}","final_contigs.gff")
  output:
    os.path.join(config["results_path"],"annotation","{group}","final_contigs.features.gff")
  shell:
    """
    python source/utils/parse_prodigal_gff.py {input} > {output}
    """

rule featurecount_pe:
  input:
    gff=os.path.join(config["results_path"],"annotation","{group}","final_contigs.features.gff"),
    bam=os.path.join(config["results_path"],"assembly","{group}","mapping","{sample}_{run}_pe"+POSTPROCESS+".bam")
  output:
    os.path.join(config["results_path"],"assembly","{group}","mapping","{sample}_{run}_pe.fc.tab"),
    os.path.join(config["results_path"],"assembly","{group}","mapping","{sample}_{run}_pe.fc.tab.summary")
  threads: 4
  params: tmpdir = config["tmpdir"]
  shell:
    """
    featureCounts -a {input.gff} -o {output[0]} -t CDS -g gene_id -M -p -B -T {threads} --tmpDir {params.tmpdir} {input.bam}
    """

rule featurecount_se:
  input:
    gff=os.path.join(config["results_path"],"annotation","{group}","final_contigs.features.gff"),
    bam=os.path.join(config["results_path"],"assembly","{group}","mapping","{sample}_{run}_se"+POSTPROCESS+".bam")
  output:
    os.path.join(config["results_path"],"assembly","{group}","mapping","{sample}_{run}_se.fc.tab"),
    os.path.join(config["results_path"],"assembly","{group}","mapping","{sample}_{run}_se.fc.tab.summary")
  threads: 4
  params: tmpdir = config["tmpdir"]
  shell:
    """
    featureCounts -a {input.gff} -o {output[0]} -t CDS -g gene_id -M -T {threads} --tmpDir {params.tmpdir} {input.bam}
    """

def get_fc_files(wildcards):
  g = wildcards.group
  files = []
  for sample in assemblyGroups[g].keys():
    for run in assemblyGroups[g][sample].keys():
      if "se" in assemblyGroups[g][sample][run].keys():
        files.append(os.path.join(config["results_path"],"assembly",g,"mapping",sample+"_"+run+"_se.fc.tab"))
      else:
        files.append(os.path.join(config["results_path"],"assembly",g,"mapping",sample+"_"+run+"_pe.fc.tab"))
  return files

rule aggregate_normalize_featurecount:
  """Aggregates feature count files and performs TPM normalization"""
  input:
    count_files = lambda wildcards: get_fc_files(),
    gff_file = os.path.join(config["results_path"],"annotation","{group}","final_contigs.features.gff"),
    sample_info = os.path.join(config["intermediate_path"],"preprocess","read_lengths.tab")
  output:
    count = os.path.join(config["results_path"],"annotation","{group}","fc.count.tab"),
    tpm = os.path.join(config["results_path"],"annotation","{group}","fc.tpm.tab")
  params:
    script = "source/utils/tpm.py"
  run:
    temp_files = []
    dirname = os.path.expandvars(config["tmpdir"])
    for file_num, f in enumerate(input.count_files, start=1):
      name = os.path.basename(f)
      sample_run = name.rstrip("_[ps]e.fc.tab")
      temp_file = os.path.join(dirname,"{}-{}-fc".format(wildcards.group,sample_run))
      temp_files.append(temp_file)
      with open(f, 'r') as fh_in, open(temp_file, 'w') as fh_out:
        if file_num == 1:
          fh_out.write("{}\t{}\n".format("gene_id",sample_run))
        else:
          fh_out.write("{}\n".format(sample_run))

        for line_num, line in enumerate(fh_in):
          line = line.rstrip()
          if line[0]=="#" or line_num < 2: continue
          items = line.split("\t")
          gene_id, contig, count = items[0], items[1], items[-1]
          gene = contig+"_"+gene_id.split("_")[-1]
          if file_num == 1:
            fh_out.write("{}\t{}\n".format(gene_id,count))
          else:
            fh_out.write("{}\n".format(count))
    files = " ".join(temp_files)

    # Paste count files
    shell("paste -d '\t' {files} > {output.count}")

    # Normalize count files using TPM
    for file_num, f in enumerate(temp_files, start=2):
      tpm_temp = f+".tpm"
      shell("cut -f1,{file_num} {output.count} > {f}")
      if file_num == 2:
        shell("python {params.script} -c {f} --gff {input.gff_file} -i {input.sample_info} > {tpm_temp}")
      else:
        shell("python {params.script} -c {f} --gff {input.gff_file} -i {input.sample_info} | cut -f3 > {tpm_temp}")
      shell("mv {tpm_temp} {f}")

    # Paste normalized files
    shell("paste -d '\t' {files} > {output.tpm}")
    shell("rm {files}")

rule make_gene_id_map:
  input:
    gff = os.path.join(config["results_path"],"annotation","{group}","final_contigs.features.gff")
  output:
    gff = os.path.join(config["results_path"],"annotation","{group}","gene_idmap.tab")
  run:
    import pandas as pd
    gff_df = pd.read_csv(input.gff, sep="\t", usecols=[0,8], names=["contig","gene_id"])
    # Create mapping of entries between prodigal and htseq
    gff_df["prodigal_id"] = [gff_df.iloc[i]["contig"]+"_"+gff_df.iloc[i]["gene_id"].split("_")[-1] for i in gff_df.index]
    gff_df["htseq_id"] = [gff_df.iloc[i]["gene_id"].split(" ")[-1] for i in gff_df.index]
    gff_df.drop("gene_id",axis=1,inplace=True)
    gff_df.to_csv(output.gff, sep="\t", index=False)

def read_htseq_df(f,gff_df):
  df = pd.read_csv(f, sep="\t", index_col=0)
  df = pd.merge(gff_df,df,left_on="htseq_id",right_index=True)
  df.index = df.prodigal_id
  df.drop(["htseq_id","prodigal_id"], axis=1, inplace=True)
  return df

def process_and_sum(q_df,annot_df):
  annot_q_df = pd.merge(annot_df, q_df, left_index = True, right_index = True)
  annot_q_df.columns = ["feature"]+list(annot_q_df.columns)[1:]
  # Calculate abundance of "Unclassified" category. That is calculate total abundance of ORFs that cannot be annotated
  # with this database.
  annot_q_unc = pd.DataFrame(q_df.loc[set(q_df.index).difference(set(annot_q_df.index))].sum(),columns=["Unclassified"]).T
  feature_q_sum = annot_q_df.groupby("feature").sum()
  # Add the unclassified category to allow for proper calculation of relative abundances
  feature_q_sum = pd.concat([feature_q_sum,annot_q_unc])
  feature_q_sum.index.name = "feature"
  return feature_q_sum

rule sum_to_features:
  input:
    gff = os.path.join(config["results_path"],"annotation","{group}","gene_idmap.tab"),
    count = os.path.join(config["results_path"],"annotation","{group}","fc.count.tab"),
    norm = os.path.join(config["results_path"],"annotation","{group}","fc.tpm.tab"),
    annot_file = os.path.join(config["results_path"],"annotation","{group}","{db}.parsed.tab")
  output:
    count = os.path.join(config["results_path"],"annotation","{group}","{db}.parsed.count.tab"),
    norm = os.path.join(config["results_path"],"annotation","{group}","{db}.parsed.tpm.tab")
  run:
    import pandas as pd
    gff_df = pd.read_csv(input.gff, sep="\t", usecols=[1,2])

    annot_df = pd.read_csv(input.annot_file, sep="\t", index_col=0, usecols=[0,1], header=0)

    norm_df = read_htseq_df(input.norm,gff_df)
    count_df = read_htseq_df(input.count,gff_df)

    feature_norm_sum = process_and_sum(norm_df,annot_df)
    feature_count_sum = process_and_sum(count_df,annot_df)

    if wildcards.db == "EGGNOG":
      feature_norm_sum.rename(index = lambda x: x.lstrip("NOG").lstrip(".").split(".")[0], inplace=True)
      feature_count_sum.rename(index = lambda x: x.lstrip("NOG").lstrip(".").split(".")[0], inplace=True)

    feature_norm_sum.to_csv(output.norm, sep="\t")
    feature_count_sum.to_csv(output.count, sep="\t")

rule quantify_kegg_pathways:
    input:
        count = os.path.join(config["results_path"],"annotation","{group}","EC.parsed.count.tab"),
        norm = os.path.join(config["results_path"],"annotation","{group}","EC.parsed.tpm.tab"),
        ec2path = os.path.join(config["resource_path"],"kegg_ec2path.tsv")
    output:
        count = os.path.join(config["results_path"],"annotation","{group}","KEGG_pathways.parsed.count.tab"),
        norm = os.path.join(config["results_path"],"annotation","{group}","KEGG_pathways.parsed.tpm.tab")
    run:
        import pandas as pd, numpy as np
        count = pd.read_csv(input.count, sep="\t", header=0)
        norm = pd.read_csv(input.norm, sep="\t", header=0)
        ec2path = pd.read_csv(input.ec2path, sep="\t", header=0)

        count_path = pd.merge(ec2path,count,left_on="enzyme",right_on="feature", how="right")
        count_path.fillna("Unclassified",inplace=True)
        count_path.drop(["enzyme","feature"],axis=1,inplace=True)
        count_path_m = count_path.groupby(["pathway","Cat1","Cat2","name"]).mean().reset_index()

        norm_path = pd.merge(ec2path,norm,left_on="enzyme",right_on="feature", how="right")
        norm_path.fillna("Unclassified",inplace=True)
        norm_path.drop(["enzyme","feature"],axis=1,inplace=True)
        norm_path_m = norm_path.groupby(["pathway","Cat1","Cat2","name"]).mean().reset_index()

        count_path_m.to_csv(output.count, sep="\t", index=False)
        norm_path_m.to_csv(output.norm, sep="\t", index=False)

rule quantify_cog_categories:
  input:
    count = os.path.join(config["results_path"],"annotation","{group}","EGGNOG.parsed.count.tab"),
    norm = os.path.join(config["results_path"],"annotation","{group}","EGGNOG.parsed.tpm.tab"),
    annotations=os.path.join(config["hmmsearch"]["dbpath"],"EGGNOG.annotations.tsv"),
    func_cat = os.path.join(config["hmmsearch"]["dbpath"],"COG_functional_categories.txt")
  output:
    count = os.path.join(config["results_path"], "annotation", "{group}","EGGNOG.parsed.categories.count.tsv"),
    norm = os.path.join(config["results_path"], "annotation", "{group}","EGGNOG.parsed.categories.norm.tsv")
  run:
    header = ["TaxonomicLevel","GroupName","ProteinCount","SpeciesCount","COGFunctionalCategory","ConsensusFunctionalDescription"]
    annotations = pd.read_csv(input.annotations, sep="\t", header=None, names=header, dtype=str)
    func_cats = pd.read_csv(input.func_cat, sep="\t", header=0)
    annotations = pd.merge(annotations,func_cats,right_on="COG_Letter",left_on="COGFunctionalCategory")

    df_count = pd.read_csv(input.count, sep="\t", header=0, index_col=0)
    df_norm = pd.read_csv(input.norm, sep="\t", header=0, index_col=0)
    samples = list(df_norm.columns[1:])

    df_count_cats = pd.merge(annotations,df_count,left_on="GroupName",right_index=True, how="right")
    df_norm_cats = pd.merge(annotations,df_norm,left_on="GroupName",right_index=True, how="right")
    df_count_cats.fillna("Unclassified", inplace=True)
    df_norm_cats.fillna("Unclassified", inplace=True)

    df_count_cats.to_csv(output.count, index=False, sep="\t")
    df_norm_cats.to_csv(output.norm, index=False, sep="\t")

rule sum_to_taxa:
  input:
    genes_from_contigs = os.path.join(config["results_path"],"annotation","{group}","taxonomy","taxonomy_genes_from_contigs.tab"),
    count = os.path.join(config["results_path"],"annotation","{group}","fc.count.tab"),
    norm = os.path.join(config["results_path"],"annotation","{group}","fc.tpm.tab"),
    gff = os.path.join(config["results_path"],"annotation","{group}","gene_idmap.tab")
  output:
    count = os.path.join(config["results_path"],"annotation","{group}","taxonomy","genes_from_contigs.count.tab"),
    norm = os.path.join(config["results_path"],"annotation","{group}","taxonomy","genes_from_contigs.tpm.tab")
  run:
    header = ["protein","superkingdom", "phylum","class","order","family","genus","species"]
    df = pd.read_csv(input.genes_from_contigs, sep="\t", index_col=0, header=None, names=header)
    gff_df = pd.read_csv(input.gff, sep="\t", usecols=[1,2])

    count_df = read_htseq_df(input.count,gff_df)
    norm_df = read_htseq_df(input.norm,gff_df)

    taxa_count = pd.merge(df,count_df,right_index=True,left_index=True)
    taxa_count_sum = taxa_count.groupby(header[1:]).sum().reset_index()
    taxa_count_sum.to_csv(output.count, sep="\t", index=False)

    taxa_norm = pd.merge(df,norm_df,right_index=True,left_index=True)
    taxa_norm_sum = taxa_norm.groupby(header[1:]).sum().reset_index()
    taxa_norm_sum.to_csv(output.norm, sep="\t", index=False)

rule eggnog2krona:
  input:
    os.path.join(config["results_path"],"annotation","{group}","EGGNOG.parsed.categories.norm.tsv")
  output:
    os.path.join(config["results_path"],"annotation","{group}","EGGNOG.krona.html")
  run:
    import pandas as pd
    f = input[0]
    dir = os.path.dirname(f)
    df = pd.read_table(f, header=0)
    samples = list(df.columns[10:])
    krona_input = []
    temp_files = []
    for sample in samples:
      df_part = df.loc[:,[sample,"COG_Category1","COG_Category2"]]
      out_f = dir+"/{}.krona_part".format(sample)
      df_part.to_csv(out_f, sep="\t", index=False)
      temp_files.append(out_f)
      krona_input.append("{},{}".format(out_f,sample))
    in_string = ' '.join(krona_input)
    files_string = ' '.join(temp_files)
    shell("ktImportText -o {output[0]} {in_string}")
    shell("rm {files_string}")

def make_krona_taxonomy_input(f,temp_dir):
  df = pd.read_table(f, header=0)
  df.drop("gene_length", axis=1, errors = "ignore", inplace=True)
  ranks = list(df.columns[0:7])
  samples = list(df.columns[7:])
  inputs = []
  for sample in samples:
      tmp_out = os.path.join(temp_dir,"{}.krona_input.txt".format(sample))
      df[[sample]+ranks].to_csv(tmp_out, sep="\t", index=False, header=False)
      inputs.append("{},{}".format(tmp_out,sample))
  return inputs

rule taxonomy2krona:
  input:
    os.path.join(config["results_path"],"annotation","{group}","taxonomy","genes_from_contigs.tpm.tab"),
    os.path.join(config["results_path"],"annotation","{group}","taxonomy","genes_from_contigs.count.tab")
  output:
    os.path.join(config["results_path"],"annotation","{group}","taxonomy","taxonomy.tpm.krona.html"),
    os.path.join(config["results_path"],"annotation","{group}","taxonomy","taxonomy.count.krona.html")
  params: temp_dir = os.path.expandvars(config["temp_path"])
  run:
    shell("mkdir -p {params.temp_dir}")
    inputs = make_krona_taxonomy_input(input[0], params.temp_dir)
    input_string = " ".join(inputs)
    shell("ktImportText -o {output[0]} {input_string}")
    for f in [item.split(",")[0] for item in inputs]:
      shell("rm {f}")

    inputs = make_krona_taxonomy_input(input[1], params.temp_dir)
    input_string = " ".join(inputs)
    shell("ktImportText -o {output[1]} {input_string}")
    for f in [item.split(",")[0] for item in inputs]:
      shell("rm {f}")



