from collections import defaultdict
import os, pandas


def parse_samplelist(f, config, PREPROCESS):
    # Handle cases where not all lines have the same number of columns
    try:
        df = pandas.read_csv(f, dtype=object, sep="\t")
    except pandas.errors.ParserError:
        with open(f) as fh: header = fh.readline()
        header = header.rstrip().split("\t")
        df = pandas.read_csv(f, dtype=object, sep="\t", names=header, skiprows=1)

    dict_shell = lambda: defaultdict(dict_shell)  # Dictionary with arbitrary number of levels
    assemblyGroups = dict_shell()
    samples = dict_shell()
    df.fillna("", inplace=True)

    for i in list(df.index):
        sample = df.iloc[i]["sampleID"]
        runID = df.iloc[i]["runID"]
        R1 = df.iloc[i]["fileName"]
        groups = []
        r2 = False

        # Initiate keys for all assembly group values
        if "assemblyGroup" in df.columns:
            groups = df.iloc[i]["assemblyGroup"].split(",")
            for g in groups:
                if g not in assemblyGroups.keys() and g != "":
                    assemblyGroups[g] = dict_shell()

        if "interleaved" in df.columns and df.iloc[i]["interleaved"]:
            # If interleaved fastq is provided, add filepaths to split fastq files and later produce these using the
            # deinterleave_fastq rule in preprocessing.rules.
            inter = R1
            R1 = os.path.join(config["intermediate_path"], "deinterleaved", "{}_{}_R1.fastq.gz".format(sample, runID))
            R2 = os.path.join(config["intermediate_path"], "deinterleaved", "{}_{}_R2.fastq.gz".format(sample, runID))
            samples[sample][runID]["interleaved"] = inter
            samples[sample][runID]["R1"] = R1
            samples[sample][runID]["R2"] = R2
            for g in groups:
                assemblyGroups[g][sample][runID]["R1"] = [os.path.join(config["intermediate_path"], "preprocess",
                                         "{}_{}_R1{}.fastq.gz".format(sample, runID, PREPROCESS))]
                assemblyGroups[g][sample][runID]["R2"] = [os.path.join(config["intermediate_path"], "preprocess",
                                         "{}_{}_R2{}.fastq.gz".format(sample, runID, PREPROCESS))]
            continue

        # Handling of paired and/or single end sequence files
        # If the sample annotation file has a 'pair' column, add the read files as 'R1' and 'R2'
        if "pair" in df.columns:
            R2 = df.iloc[i]["pair"]
            # If 'pair' actually points to a valid file, add R2 to files and pair it to R1
            if os.path.isfile(R2):
                r2 = True
                samples[sample][runID]["R1"] = R1
                samples[sample][runID]["R2"] = R2
            # If 'pair' is not a valid file, just add R1 as single end
            else:
                samples[sample][runID]["se"] = R1

            # Add filepaths to preprocessed output files for each of the read files in each of the assembly groups
            # This will be the initial input to the assembly rule
            for g in groups:
                if r2:
                    assemblyGroups[g][sample][runID]["R1"] = [os.path.join(config["intermediate_path"], "preprocess",
                                         "{}_{}_R1{}.fastq.gz".format(sample, runID, PREPROCESS))]
                    assemblyGroups[g][sample][runID]["R2"] = [os.path.join(config["intermediate_path"], "preprocess",
                                         "{}_{}_R2{}.fastq.gz".format(sample, runID, PREPROCESS))]
                else:
                    assemblyGroups[g][sample][runID]["se"] = [os.path.join(config["intermediate_path"], "preprocess",
                                        "{}_{}_se{}.fastq.gz".format(sample, runID, PREPROCESS))]

        # If there is no 'pair' column, add the single file path as 'se'
        else:
            samples[sample][runID]["se"] = R1
            for g in groups:
                assemblyGroups[g][sample][runID]["se"] = [os.path.join(config["intermediate_path"], "preprocess",
                                         "{}_{}_se{}.fastq.gz".format(sample, runID, PREPROCESS))]
    return samples, assemblyGroups

def check_sequencing_type(samples):
  types = []
  for sample in samples.keys():
    for run in samples[sample].keys():
      if "R2" in samples[sample][run].keys():
        types.append("paired")
      else:
        types.append("single")
  if len(set(types))>1: return 'mixed'
  else: return types[0]