import sys, re, argparse, operator, glob, os, subprocess
import pandas as pd

parser = argparse.ArgumentParser()
parser.add_argument('--sampleList', dest='sampleList', help="the list of samples to run", type=str, default=None, required=True);
parser.add_argument('--outputFile', dest='outputFile', help="the PDF file with plotted information", type=str, default=None, required=True);
parser.add_argument('--results_path', dest='results_path', type=str, default=None, required=True);
parser.add_argument('--res_set', dest='res_set', type=str, default=None, required=True, help="the name of the results outputfolder, eg the name of the results set");
parser.add_argument('--min_coverage', dest='min_coverage', default=False, type=float, required=True);
parser.add_argument('--verbose', dest='verbose', default=False, action="store_true");
args = parser.parse_args();
resF = args.results_path
resS = args.res_set
tsvFile = args.outputFile
tsvFile = tsvFile.replace("pdf","tsv") # will first generate a tsv file, then a plot this information to a pdf file...

def main():
    # read in the sampleList as a pandas dataframe so the order of columns is not determined
    samplesTable=pd.read_csv(args.sampleList, dtype={'runID': object}, header=0, sep="\t")
    with open(tsvFile, 'w') as outFile:
        for index, row in samplesTable.iterrows():
            ind = row['sampleID']
            run = row['runID']
            sample = ind + "_" + run

            if os.path.exists(resF+"/kraken_sample_report/"+sample):
                # step 1 = kraken sample report number of found species = number of lines (-1 for unclassified)
                count1 = _getCount("cat "+resF+"/kraken_sample_report/"+sample+" | wc -l | egrep -o [0-9]+")-1
                if(args.verbose):
                    print("step1: count is "+str(count1)+" for sample "+sample)
                # step 2 = pathogen filtering - how many passed this filter? (eg are strains and have at least '10' reads) (-1 for the header)
                count2 = _getCount("cat "+resF+"/"+resS+"/filter_taxa/"+sample+"*filtered.tab | wc -l | egrep -o [0-9]+ ")-1
                if(args.verbose):
                    print("step2: count is "+str(count2)+" for sample "+sample)
                # step 3 = can do bowtie align to pathogen
                #count3 = _getCount("grep -P \""+ind+"\\t"+run+"\" "+resF+"/"+resS+"/aggregated/bam_stats.tsv | wc -l ")
                count3 = _getCount("cat "+resF+"/"+resS+"/remove_non_aligning_taxa/"+sample+".keptTaxa | wc -l | egrep -o [0-9]+")
                if(args.verbose):
                    print("step3: count is "+str(count3)+" for sample "+sample)
                # step 4 = successful mapDamage calculations
                count4 = _getCount("grep -P \""+ind+"\\t"+run+"\" "+resF+"/"+resS+"/aggregated/ancient_stats.tsv | wc -l ")
                if(args.verbose):
                    print("step4: count is "+str(count4)+" for sample "+sample)
                # step 5 = is Ancient
                count5 = _getCount("grep -P \""+ind+"\\t"+run+"\" "+resF+"/"+resS+"/aggregated/ancient_stats.tsv | grep Yes | wc -l ")
                if(args.verbose):
                    print("step5: count is "+str(count5)+" for sample "+sample)
                # coverage calculations
                count6 = _getCount("grep -P \""+ind+"\\t"+run+"\" "+resF+"/"+resS+"/aggregated/coverage_stats.tsv | cut -f 4 | awk '$1 > "+str(args.min_coverage)+" { print }' | wc -l ")
                if(args.verbose):
                    print("step6: count is "+str(count6)+" for sample "+sample)

                # print all the counts to the summary file!
                outFile.write(sample+"\t"+str(count1)+"\t"+str(count2)+"\t"+ \
                    str(count3)+"\t"+str(count4)+"\t"+str(count5)+"\t"+str(count6)+"\n")
            else:
                outFile.write(sample+"\t0\t0\t0\t0\t0\t0\n")
    makePlot()

def makePlot():
    Rcode = "inputData = read.table(\""+tsvFile+"\", sep=\"\\t\", header=FALSE); \n"+ \
        "maxX = ncol(inputData); \n" + \
        "maxY = max(inputData[,2:maxX]); \n" + \
        "nrOfSamples = nrow(inputData); \n" + \
        "cols=rep(x=c(\"red\",\"green\",\"blue\",\"magenta\",\"yellow\",\"brown\",\"purple\",\"slateblue\",\"seagreen\",\"darkgreen\",\"sienna\",\"violetred\",\"violet\",\"wheat\",\"aliceblue\",\"gray\",\"azure\",\"coral\",\"cyan\",\"darkgoldenrod\",\"black\",\"beige\",\"darkcyan\",\"deeppink\",\"firebrick\",\"gainsboro\"),times=3); " + \
        "labs=array(data=NA,dim=nrOfSamples); \n" + \
        "pdf(file=\""+args.outputFile+"\"); \n" + \
        "plot(x=1,y=1,col=\"white\",xlim=c(0,maxX+1),ylim=c(0,maxY),xlab=\"Pipeline step\",ylab=\"Number of species considered\",xaxt=\"n\"); \n" + \
        "for(i in 1:nrOfSamples){ \n" + \
            "lines(x=seq(from=1,to=maxX-1,by=1), y=inputData[i,2:maxX], col=cols[i]); \n" + \
            "labs[i] = paste(inputData[i,1],\":\",inputData[i,maxX],\"taxa in the end\"); \n" + \
        "}\n" +\
        "steps=c(\"Kraken\",\"Filtering\",\"Alignment\",\"mapDamage\",\"isAncient\",\"Coverage\"); \n" + \
        "text(x=seq(from=1,to=(maxX-1),by=1), y=rep(x=600,times=(maxX-1)), labels=steps, col.lab=\"magenta\", cex.lab=0.5,srt=90);  \n" + \
        "dev.off();"
    with open("Rcode.R", 'w') as RcodeFile:
        RcodeFile.write(Rcode)
    os.system("Rscript Rcode.R");


def _getCount(cmd):
    if(args.verbose):
        print("Command to run is "+cmd)
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
    (count, err) = p.communicate()
    p.wait()
    count = int(count.decode("utf-8").rstrip())
    return(count)



if __name__ == "__main__":
    main()
