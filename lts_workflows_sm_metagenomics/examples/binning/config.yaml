workdir: .
sample_list: examples/binning/sample_annotation.tab
results_path: results/examples/binning
intermediate_path: results/examples/binning/intermediate
temp_path: temp
scratch_path: temp
resource_path: resources
report_path: results/examples/binninb/report
benchmark_path: results/examples/benchmark
pipeline_config_file: results/examples/binning/pipeline_config.txt

################
### SETTINGS ###
################

#####################
### PREPROCESSING ###
#####################
# trimmomatic
trimmomatic:
  run_trimmomatic: True
  trimmomatic_home: ""
  trim_adapters: True
  pe_adapter_params: "2:30:15"
  se_adapter_params: "2:30:15"
  pe_pre_adapter_params: ""
  pe_post_adapter_params: "LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:31"
  se_pre_adapter_params: ""
  se_post_adapter_params: "LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:31"
# cutadapt
cutadapt:
  run_cutadapt: False
  adapter_sequence: AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC # Adapter sequence for trimming. Shown here is for Illumina TruSeq Universal Adapter.
  rev_adapter_sequence: AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT
# fastuniq
fastuniq:
  run_fastuniq: False
# filter phiX
phix_filter:
  run_phix_filter: True
# SortMeRNA
sortmerna:
  run_sortmerna: False
  # Sortmerna produces files with reads aligning to rRNA ('rRNA' extension) and not aligning to rRNA ('non_rRNA') extension
  # Which reads should be used for downstream analyses
  keep: 'non_rRNA'
  # Remove filtered reads (i.e. the reads NOT specified in 'keep:')
  remove_filtered: False
  dbs: ["rfam-5s-database-id98.fasta","rfam-5.8s-database-id98.fasta","silva-arc-16s-id95.fasta","silva-arc-23s-id98.fasta","silva-bac-16s-id90.fasta","silva-bac-23s-id98.fasta","silva-euk-18s-id95.fasta","silva-euk-28s-id98.fasta"]
  # 'paired_in' puts both reads in a pair into the 'rRNA' bin if one of them aligns (i.e. more strict)
  # 'paired_out' puts both reads in the 'other' bin
  paired_strategy: "paired_in"
  params: "--num_alignments 1 -v"

###############################
### Assembly and annotation ###
###############################

assembly:
  threads: 16
  keep_intermediate: False
  # To use the 'Megahit ep k21-k91' setting from the CAMI paper (Sczyrba et al 2017) set additional_settings: to '--min-contig-len 300 --prune-level 3'
  additional_settings: '--min-contig-len 300 --prune-level 3'

hmmsearch:
  run_hmmsearch: False
  threads: 8
  # HMM database(s) to run predicted proteins against
  hmmdbs: ["TIGRFAM","PFAM","EGGNOG"]
  tigrfam_threads: 4
  pfam_threads: 4
  eggnog_threads: 16
  dbpath: resources/hmm

# BLAST database for taxonomic annotation.
# Should be formatted using diamond.
taxonomic_annotation:
  run_taxonomic: False
  diamond_dbpath: resources/diamond
  # dbtype options are: nr, uniref50, uniref90, uniref100
  # nr typically has around 432,300,014 sequences
  # uniref100 has around 114,464,072 sequences
  # uniref90 has around 59,207,328 sequences
  # uniref50 has around 24,713,224 sequences
  dbtype: uniref100
  threads: 16

# Taxonomic information
taxdb: resources/taxonomy

# Infernal ncRNA detection
infernal:
  run_infernal: False
  threads: 2
  dbpath: resources/infernal

###############
### BINNING ###
###############
binning:
  run_binning: True
  map_dir: "results/binning/map"
  bin_dir: "results/binning/bins/"
  min_contig_length: 1000
  threads: 8

# Do QA on produced bins using Checkm?
checkm:
  run_checkm: True
  dir: "resources/checkm"
  marker_taxon: "Prokaryote"
  marker_rank: "life"
  reduced_tree: True
  tree_params: ""
  good_bin_threshold:
    completeness: 80
    contamination: 5
    max_genome_size: None
    min_genome_size: None

###############
### MAPPING ###
###############
bowtie2:
  threads: 16

######################
### POSTPROCESSING ###
######################
# Whether the picard tool MarkDuplicates should be run to remove duplicates after mapping.
# Set to False if doing metatranscriptomics.
picard:
  run_markduplicates: False
  picard_jar: ""
  picard_path: ""

####################################
### REFERENCE BASED READ MAPPING ###
####################################
reference_map:
  run_reference_map: True
  dir: "resources/refmap"

  ### Filtering of genomes
  # As a first step, centrifuge is used to get an estimate of the actual genomes present in each sample
  # The parameters below specify the minimum requirements for including genomes in downstream mapping

  # Set minimum read count required during the pre-filtering step to include a genome
  # Note that this is not uniquely classified reads
  min_read_count: 500

  # Minimum fraction of species max
  # centrifuge reports multiple mappings per read so several genomes of the same species may be mapped
  # to filter out false-positive genomes we normalize the read count to the genome with the maximum assignments
  # per species, then filter away genomes with a fraction of read assignments lower than the parameter below
  # At 0.1, genomes with read assignments <10% of the maximum for the species will be removed
  min_fraction_of_species_max: 0.1

######################
### CLASSIFICATION ###
######################
# Path to where the classifier (Kraken/Centrifuge/Kaiju) database will be stored. If possible, this
# should be on a fast disk.
classifier_db_path: resources/classify_db

#############
### KAIJU ###
#############

kaiju:
  run_kaiju: False

##############
### KRAKEN ###
##############
kraken:
  run_kraken: False

  # Threshold used in kraken-filter. This is the minimal proportion of a read covered
  # in kmers mapping to a given taxa for it to be assigned to that taxa. 0.2 was
  # selected as a good compromise between specificity and sensitivity in the Kraken
  # paper.
  filter_threshold: 0.2

  # The length of kmers used in Kraken. Impacts the memory requirements for jellyfish,
  # but also the specificity. 31 bp is the default.
  kmer_length: 31

  min_coverage_estimated_from_kraken_read_count_only: 0.0001 # was manually set to 0 in the filtering.rules file before...

#######################
### KRAKEN DATABASE ###
#######################
# This section has settings for what to download and use to generate the kraken classifier database.

####### UPDATE #######
# As of version 1.0 KRAKEN no longer relies on GI numbers.

  # The kraken database is generated from 3 potential sources specified below:
  # 1. kraken_custom_libraries
  #   Dictionary with custom FASTA files that should be appended to the database. The
  #   key is the library name which will be used in the database. The value is a list,
  #   where the first element is the path, the second the pattern to download and the
  #   third is an optional Bash command to use for filtering. The string plus space
  #   is prepended to the file name, and the whole command must write results to stdout.
  #   Use "" if not applicable. Any files matching the pattern will be assumed to contain
  #   sequences in FASTA format. If the file name ends with ".gz" it will be decompressed
  #   before being appended to the database.
  #   For keys 'Viral', 'Archaea', 'Bacteria' and 'Plasmid' these should be the same as
  #   the lower-case equivalents in kraken_libraries.
  # UNCOMMENT below to use
  kraken_custom_libraries:
    #Mammals: ['ftp://ftp.ncbi.nih.gov/refseq/release/vertebrate_mammalian','vertebrate_mammalian.*.genomic.fna.gz','bash source/utils/filter_human.sh']
    #Bacteria: ['ftp://ftp.ncbi.nih.gov/refseq/release/bacteria','bacteria.*.genomic.fna.gz',""]
    #Fungi: ['ftp://ftp.ncbi.nih.gov/refseq/release/fungi','fungi.*.genomic.fna.gz',""]
    #Protozoa: ['ftp://ftp.ncbi.nih.gov/refseq/release/protozoa','protozoa.*.genomic.fna.gz',""]
    #Archaea: ['ftp://ftp.ncbi.nih.gov/refseq/release/archaea','archaea.*.genomic.fna.gz',""]
    #Viral: ['ftp://ftp.ncbi.nih.gov/refseq/release/viral','viral.*.genomic.fna.gz',""]
    #Plasmid: ['ftp://ftp.ncbi.nih.gov/refseq/release/plasmid','plasmid.*.genomic.fna.gz',""]

  # 2. kraken_libraries
  #   This can be a list containing any combination of "archaea", "bacteria", "plasmid","viral" and "human".
  #   The list is used in the built-in kraken-build --download-library <library_name> --db $DBNAME
  kraken_libraries: [] # ['bacteria', 'archaea', 'viral']

  # 3. kraken_assembly_lists
  #   This is a list of filenames with assemblies for sets of taxa. Each file is parsed and assemblies are sorted by level
  #   of completeness keeping only the best one if there are several assemblies. In addition, only one genome is kept per
  #   species if there are many strains and assemblies. Each assembly is then added to kraken_custom_libraries.
  kraken_assembly_lists: ['resources/ncbi/assembly_lists/assembly_summary_bacteria.txt', 'resources/ncbi/assembly_lists/assembly_summary_viral.txt'] #['resources/ncbi/assembly_lists/assembly_summary_bacteria.txt','resources/ncbi/assembly_lists/assembly_summary_archaea.txt','resources/ncbi/assembly_lists/assembly_summary_fungi.txt','resources/ncbi/assembly_lists/assembly_summary_human.txt','resources/ncbi/assembly_lists/assembly_summary_protozoa.txt']

  # Special parameters for kraken-build.
  kraken_build_params: '--work-on-disk'

# List of NCBI GIs which have been deprecated. Used when generating per-species fasta
# files from the concatenated reference.
deprecated_gi: resources/deprecatedGI.tab

#####################
### Centrifuge DB ###
#####################
centrifuge:
  run_centrifuge: True

  # Minimum score for classifications by centrifuge.
  # Because centrifuge doesn't have a filtering algorithm, we use this min_score to filter results.
  min_score: 75

  # Maximum number of assignments per read
  # By default this is set to 5 in centrifuge, increase to show more specific assignments
  max_assignments: 10

  # Mask low complexity regions using dustmasker
  mask_low_complexity: True

  # What domain libraries to download?
  # This list may contain 'bacteria', 'viral', 'archaea', 'fungi', 'protozoa', 'invertebrate', 'plant', 'vertebrate_mammalian', 'vertebrate_other'
  domains: ['archaea', 'fungi']

  # List of taxonomic ids to download. If using this option, be sure to set the list of domains (above) to match
  # the domains contained in your taxid list
  taxidlist:

  # What assembly level to include libraries for?
  # This list may contain "Complete Genome","Contig","Chromosome","Scaffold"
  assembly_level: ["Complete Genome"]

  # Only download one genome assembly per unique taxid
  unique_taxid: False

  # Summarize at rank
  # Specify a taxonomic rank to summarize the database at (total sequences and total size).
  # Leave blank to not do summarization
  summarize_rank: superkingdom

#######################
###### SCREENING ######
#######################
screening:
  run_screening: False
# This section deals specifically with steps in the 'screen' snakemake target.

# What classifier to use
# the options are [ 'kraken', 'centrifuge']
classifier: 'centrifuge'

#################
### FILTERING ###
#################
# In order to allow the same classification (which is the time, and resource,
# heavy step) to be used with different filtering parameters, such as coverage,
# min number of k-mer to consider a match, ancient or not, etc
# the results from the downstream filtering, if applicable, will be written to
# the following sub-directory in the results_path folder
tax_filter_name: screening_filter

# The workflow contains a number of rules which perform per species analyses, e.g.
# mapping of reads to the relevant genome and generate coverage plots. Since the
# number of species found in a metagenomic sample can be very large, there are several
# filtering steps used to reduce the number of species to analyse more in detail.

# Whitelist of NCBI taxonomy IDs to include when filtering Kraken results. Use
# 'scientificName.tab' to include all taxa in the NCBI taxonomy.
species_whitelist: resources/ncbi/scientificName.tab

# Number of k-mers required for the taxa (in the downstream filterings first step).
min_kmer_count: 5

# Minimum read quality
min_read_quality: 30                   # set to 0 to ignore this if your samples are ancient

# Minimal coverage of a genome for it to be considered a potential hit.
min_coverage_kept:   0.001   # this should be set very allowing as none of the species that fail this will be "kept"

# BOWTIE parameters used when aligning the potential reads for a given taxa to that particular .fasta file
bowtie_align_to_taxa: '--quiet --end-to-end --very-sensitive'

# It is also possible to take the bam file aligned to the specific taxa,
# make a fastq of it and then align that to the full database to get an idea
# of specificity of your match...
# Note that if you set this to True, then please increase the 'min_coverage_kept'!
align_to_full_database: True
bowtie_align_to_full_database: '--quiet --very-sensitive'

#######################################
### ANCIENT (eg MapDamage) settings ###
#######################################
# - mapDamage (https://ginolhac.github.io/mapDamage/)
# If mapDamage should be run to filter out species without significant dna degradation.
mapdamage_run_type: 'skip'             # options: quick (dont do the bayesian estimations), skip (eg you want all not only ancient taxa), complete (mapDamage with bayesian estimations)
mapdamage_freq: 0.05                   # When is a sample considered ancient?
mapdamage_prob: 0.6                    # When is a sample considered ancient?
mapdamage_sequence_length: 6           # the default from map damage is 12, but as we only look at the first and last position... (min req 4!)
mapdamage_starting_moves: 5000         # the default from map damage is 10.000
mapdamage_number_of_iterations: 30000  # the default from map damage is 50.000
mapdamage_run_in_failsafe_mode: False  # this is the place where the pipeline fails the most (!) so make it possible to easily get the error message to console by setting this to False...

# mapDamage2 requires python 2.7 while Snakemake requires python 3. This is the
# name of a conda environment used for running mapDamage.
conda_mapdamage: mapdamage
