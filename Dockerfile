FROM scilifelablts/lts-workflows
LABEL maintainer="Rasmus Agren <rasmus.agren@scilifelab.se>"
LABEL vendor="Science for Life Laboratory"

RUN apt-get update && apt-get -y dist-upgrade
RUN apt-get install -y --no-install-recommends \
    bzip2 \
    unzip \
    nano \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/


##################################################
# R packages
##################################################
RUN install2.r --error \
    gam \
    ggplot2 \
    inline \
    RcppGSL \
    dplyr \
    && rm -rf /tmp/downloaded_packages/ /tmp/*.rds

# This is mounted as a volume to ensure fast IO when using "shadow"
#VOLUME lts-workflows-sm-metagenomics/.snakemake/shadow

#RUN chmod -R a+rw /home/lts-workflows-sm-metagenomics

# Add the repo for the workflow
ADD . /tmp/repo

RUN conda update conda \
    && conda env update --name root --file /tmp/repo/lts_workflows_sm_metagenomics/envs/environment.yaml \
    && conda env update --name py2.7 --file /tmp/repo/lts_workflows_sm_metagenomics/envs/environment-27.yaml \
    && conda clean --all

#CMD ["lts-workflows-sm-metagenomics"]
